import React from "react";
import { StyleSheet } from "react-native";
import { StackNavigator, SafeAreaView } from "react-navigation";
import {
  Container,
  Header,
  Title,
  Left,
  Icon,
  Right,
  Button,
  Body,
  Content,
  Text,
  Card,
  CardItem
} from "native-base";

class HomeScreen extends React.Component {
  render() {
    console.log("props", this.props);

    // do some destructuring of props for better reading
    const { navigation } = this.props;

    return (
      <Container>
        <Content padder>
          <Text>This is the main screen</Text>
          <Button
            onPress={() => navigation.navigate("Photos", { name: "Jane" })}
            style={styles.btn}
          >
            <Text>Photos</Text>
          </Button>
          <Button
            onPress={() => navigation.navigate("Settings", { test: "hello!" })}
            style={styles.btn}
          >
            <Text>Settings</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

HomeScreen.navigationOptions = {
  title: "Main"
};

const styles = StyleSheet.create({
  btn: {
    margin: 20
  }
});

export default HomeScreen;
