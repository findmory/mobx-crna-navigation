import React from "react";
import { Alert } from "react-native";
import {
  Container,
  Header,
  Title,
  Left,
  Icon,
  Right,
  Button,
  Body,
  Content,
  Text,
  Card,
  CardItem
} from "native-base";

export default class Settings extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    let title = "settings";
    let headerRight = (
      <Button transparent info onPress={params.btnClick}>
        <Text>Info</Text>
      </Button>
    );
    return { title, headerRight };
  };
  // we can only set the function after the component has been initialized
  componentDidMount() {
    this.props.navigation.setParams({
      btnClick: this._showAlert
    });
  }

  _showAlert = () => {
    Alert.alert("Title", "Msg");
  };

  render() {
    // the passed in param "test" is available on this.props.navigation.state.params
    const { params } = this.props.navigation.state;
    return (
      <Container>
        <Content padder>
          <Card>
            <CardItem>
              <Body>
                <Text>This is a screen with a button in the header</Text>
                <Text>passed prop: {params.test}</Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

// moved this into the class definition so i could get access to navigation
// to customize header and button

// Settings.navigationOptions = {
//   title: "Settings",
//   headerRight: (
//     <Button
//       transparent
//       info
//       onPress={this.props.navigation.state.params.btnClick}
//     >
//       <Text>Info</Text>
//     </Button>
//   )
// };
