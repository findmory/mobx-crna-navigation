import React from "react";
import { Button, ScrollView, Text } from "react-native";
import { StackNavigator, SafeAreaView } from "react-navigation";

// components
import MyHomeScreen from "./HomeScreen";
import MyPhotosScreen from "./PhotosScreen";
import MySettingsScreen from "./SettingsScreen";

const MainStack = StackNavigator(
  {
    Home: {
      screen: MyHomeScreen
    },
    Settings: {
      screen: MySettingsScreen
    },
    Photos: {
      screen: MyPhotosScreen
    }
  },
  {
    initialRouteName: "Home"
    //mode: "modal",
    //headerMode: "none"
  }
);

export default MainStack;
