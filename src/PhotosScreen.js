import React from "react";
import { StyleSheet } from "react-native";
import { Container, Content, Text, Button, Header } from "native-base";

class PhotosScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    //let title = "Photos";
    //return { title };

    // the title header visibility to false
    // then add header manually in the component
    let header = false;
    return { header };
  };
  render() {
    console.log("props", this.props);
    return (
      <Container>
        <Header>
          <Text style={styles.text}>I made this header!</Text>
        </Header>
        <Content padder>
          <Text>Photos</Text>
          <Text>Two ways to go back here</Text>
          <Button
            danger
            small
            onPress={() => this.props.navigation.goBack(null)}
            style={styles.btn}
          >
            <Text>Back</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

// PhotosScreen.navigationOptions = {
//   title: "Photos",
//   mode: "modal",
//   header: "none"
// };

const styles = StyleSheet.create({
  btn: {
    alignSelf: "center",
    marginTop: 20
  },
  text: {
    fontWeight: "bold",
    paddingTop: 15
  }
});

export default PhotosScreen;
